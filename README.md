> Nama : Muhammad Naufal Faishal            
Kelas / Absen : 3SI3 / 27

## Laporan Praktikum 3

1. Tangkapan Layar Web Service
    >![WebService](/image/WebService.png "WebService")
1. Tangkapan Layar Metode yang saya buat
    >![Metode](/image/Method.png "Metode")
1. Tangkapan Layar Hasil
    >![Hasil](/image/Hasil.png "Hasil")

> Beberapa hal yang saya bisa pahami dari praktikum ini adalah
>>Kita bisa membuat Web Service dari java yang mana pada praktikum kali ini kita membuat beberapa method dan method-method tersebut nantinya akan kita gunakan dari sisi client
>>
>>Beberapa method yang saya buat pada praktikum kali ini adalah add, hello, dan square.
Method add digunakan untuk menjumlahkan 2 bilangan. Method hello digunakan untuk memunculkan kata "Hello!". Dan method square digunakan untuk mencari kuadrat dari bilangan yang diinputkan.
>>
>>Setelah kita membuat web service, kita bisa menggunakan method-method yang ada di dalamnya dengan mengaksesnya menggunakan WSDL atau Web Service Description Language. Pada praktikum kali ini kita membuat sebuah java application yang lalu kita menambahkan web service yang kita buat tadi ke web service reference. Beberapa pilihan method tadi pun akan muncul dan kita bisa langsung drag and drop untuk menggunakannya.
>>
>>Pada contoh di atas saya mencoba menggunakan metode "add" dengan memberikan bilangan 5 dan 8, serta saya juga memanggil metode "square" dengan memberikan bilangan 9. Dapat dilihat hasilnya bahwa keluar angka 13 dan 81 yang mana itu membuktikan bahwa program berjalan dengan baik.
